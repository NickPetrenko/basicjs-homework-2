// 1. Створіть змінну "username" і присвойте їй ваше ім'я. Створіть змінну "password" 
// і присвойте їй пароль (наприклад, "secret"). Далі ми імітуємо введеня паролю користувачем. 
// Отримайте від користувача значення його паролю і перевірте, чи співпадає воно зі значенням 
// в змінній "password". Виведіть результат порівнння в консоль.

let username = "Nick";
let password = "secret";

let userPassword = prompt("Введіть ваш пароль:");

if (userPassword === password) {
  console.log("Пароль вірний!");
} else {
  console.log("Пароль невірний!");
}

// 2. Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y та запишіть 
// присвойте їй значення 3. Виведіть результати додавання, віднімання, множення та ділення 
// змінних x та y у вікні alert.

let x = 5;
let y = 3;

let addition = x + y;
let subtraction = x - y;
let multiplication = x * y;
let division = x / y;

alert("Додавання: " + addition + "\n" +
      "Віднімання: " + subtraction + "\n" +
      "Множення: " + multiplication + "\n" +
      "Ділення: " + division);